package main

import "fmt"

func main() {

	opc := 0

	var num1, num2, res int
	fmt.Println("Seleccione una opcion")
	fmt.Println("\n1-Suma, \n2-Resta, \n-3 Multiplicacion. \n4-Dicision")
	fmt.Scanln(&opc)
	fmt.Println("Ingresa el primer valor :")
	fmt.Scanln(&num1)

	fmt.Println("Ingresa el segundo valor :")
	fmt.Scanln(&num2)

	if opc == 1 {
		res = num1 + num2
		fmt.Println("La suma es \n: ", res)
	} else if opc == 2 {
		res = num1 - num2
		fmt.Println("La resta es \n: ", res)
	} else if opc == 3 {
		res = num1 * num2
		fmt.Println("La multiplicacion es \n: ", res)
	} else if opc == 4 {
		res = num1 / num2
		fmt.Println("La division es \n: ", res)
	}

}