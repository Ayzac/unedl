LIST P=16F84A
INCLUDE <P16F84A.INC>

CBLOCK 	0X0C
TIEMPO
ENDC

	ORG0
Inicio
	bsf 		STATUS, RP0
	clrf 		PORTB
	bcf 		STATUS, RP0

Principal
	bcf PORTB,0
	call Retardo_100ms
	bsf PORTB,0
	call Retardo_100ms

	
	bcf PORTB,1
	call Retardo_100ms
	bsf PORTB,1
	call Retardo_100ms

	bcf PORTB,2
	call Retardo_100ms	
	bsf PORTB,2
	call Retardo_100ms
	
	goto 	Principal
	
;Subrutinas Retardo
	INCLUDE<RETARDOS.INC>

END 