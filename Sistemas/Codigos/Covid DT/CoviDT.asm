LIST P=16F84A
INCLUDE <P16F84A.INC>

ORG 0

Inicio
	clrf	PORTB
	bsf		STATUS,RP0
	clrf	PORTB
	movlw	b'00011111'
	movwf	PORTA
	bcf		STATUS,RP0
	
Principal
	movf	PORTA,W  	
	andlw	b'00001111'
	call PrenderLeds
	movwf PORTB
	goto Principal
;subrutina

PrenderLeds	
addwf	PCL,F
DT 1h,3h,4h,7Fh,0xC,7Fh,7Fh,7Fh,30h,7Fh,7Fh,7Fh,7Fh,7Fh,7Fh,7Fh

END