LIST P=16F84A
INCLUDE <P16F84A.INC>

CBLOCK 0X0C
ENDC

ORG 0
Inicio 
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
	
Principal
	movlw (MensajeFin-MensajeInicio); W=Longitud del mensaje
	subwf PORTA,W
	btfsc STATUS,C
	
    goto MensajeMenor
	movf PORTA,W
	call LeeCaracter
	call ASCII_a_7Segmentos	
	goto Salida
	
MensajeMenor
movlw b'10000000';caracter fuera de rango

Salida
movwf PORTB;mostrar en la barra de leds
goto Principal


;Subrutina LeeCaracter
LeeCaracter
addwf PCL,F; sumar contador del programa

MensajeInicio
DT "ZAC LOPEZ"
MensajeFin
INCLUDE<DISPLAY_7S.INC>

END