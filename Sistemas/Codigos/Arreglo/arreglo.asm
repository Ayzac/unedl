section .data
msg db "Ingrese los 5 numeros :",0xA
len equ $- msg

msg1 db "El mayor es:"
len1 equ $- msg1

arre db 0,0,0,0,0
la equ $ - arre

salto db 0xA
lsalto equ $ - salto


section .bss
res resb 2


section .text
	global _start

_start:
	mov eax,4
	mov ebx,1
	mov ecx,msg
	mov edx,len
	int 0x80

	mov esi,arre
	mov edi,0

;leer los datos del teclado
lee:
	mov eax,3
	mov ebx,0
	mov ecx,res
	mov edx,2
	int 0x80


	mov al,[res]    ; mover el valor a al
	sub al,'0'       ;restar el caracter para poderlo evaluar

	mov [esi],al      ;Movemos el valor de al a esi

	add esi,1
	add edi,1

	cmp edi, la    ;Comparamos edi con la longitud del arreglo
	;edi < la = vl < vr
	jb lee

	mov ecx,0 
	mov bl,0   ;bl va almacenar nuestro numero mas grande


lp:  ;Recorrer el arreglo
	mov al,[arre+ecx]  ;Le suma la direccion de memoria,va a la primera ubicacion
	;y va aumentar lo que tenga ecx, que en la primera vuelta es 0

	cmp al,bl
	jb reg
	mov bl,al


reg:
	inc ecx
	cmp ecx,la
	jb lp


imprimir:
	add bl,'0'	
	mov [res],bl

	mov eax,4
	mov ebx,1
	mov ecx,msg1
	mov edx,len1
	int 0x80

	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,2
	int 0x80


	mov eax,4
	mov ebx,1
	mov ecx,salto
	mov edx,lsalto
	int 0x80

salida:
	mov eax,1
	int 0x80