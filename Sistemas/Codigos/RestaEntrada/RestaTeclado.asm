SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1

section .data
msg1 db "Ingrese el primer digito:"
len1 equ $ - msg1
msg2 db "Ingrese el segundo digito:"
len2 equ $ - msg2
msg3 db "El resultado es:"
len3 equ $ - msg3
salto db "",0xa,0xd
lensalto equ $ - salto

section .bss
num1 resb 2
num2 resb 2
res resb 1

section .text
global _start

_start:

;imprimir primer mensaje
mov eax,SYS_WRITE  ;4
mov ebx,STDOUT     ;1
mov ecx,msg1
mov edx,len1
int 0x80
;leer digito del teclado
mov eax,SYS_READ
mov ebx,STDIN
mov ecx,num1
mov edx,2
int 0x80
;imprimir segundo digito
mov eax,SYS_WRITE  ;4
mov ebx,STDOUT     ;1
mov ecx,msg2
mov edx,len2
int 0x80
;leer segundo digito
mov eax,SYS_READ
mov ebx,STDIN
mov ecx,num2
mov edx,2
int 0x80

;convertir de codigo ascII a un numero decimal
mov eax,[num1]
sub eax,'0' ;convertir a decimal

mov ebx,[num2]
sub ebx,'0'

;realiza resta
sub eax,ebx  
add eax,'0'
mov [res],eax ;guardo resultado de la resta

;imprimir

mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,msg3
mov edx,len3
int 0x80

mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,res
mov edx,1
int 0x80


;salida del programa

mov eax,SYS_EXIT
int 0x80