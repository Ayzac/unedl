LIST P=16F84A
INCLUDE<P16F84A.INC>
	CBLOCK	0x0C
	Unidades	Centenas	
	Decenas			
	ENDC
NUMERO	EQU	d'124'	
ORG 0
Inicio
	bsf		STATUS,RP0
	clrf		PORTB
	bcf		STATUS,RP0
Principal
	clrf Centenas
	clrf Decenas
	movlw NUMERO
	movwf Unidades
Resta_10Unidades
	movlw	.10
	subwf Unidades,W
	btfss STATUS,C
	goto Salida
IncrementarDecenas
	movwf	Unidades
	incf	Decenas,F
	movlw	.10
	subwf Decenas,W
	btfss STATUS,C
	goto Resta_10Unidades
	
IncrementarCentenas
	clrf Decenas
	incf Centenas,F
	goto Resta_10Unidades
Salida
	swapf	Decenas,W
	addwf	Unidades,W
	movwf	PORTB
	sleep
	end