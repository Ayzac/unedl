SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1

section .data
msg db "ingresa primer numero: ",0xa,0xd
len equ $-msg
msg2 db "ingresa segundo numero: ",0xa,0xd
len2 equ $-msg2
msg3 db "el segundo es mayor "
len3 equ $-msg3
msg4 db "el segundo es menor"
len4  equ $-msg4
msg5 db "son iguales "
len5 equ $-msg5
salto db "",0xa,0xd
lensalto equ $-salto


section .bss
n1 resb 2
n2 resb 2


section .text
	global _start
_start:
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,msg
mov edx,len
int 0x80

;leer teclado
mov eax,SYS_READ
mov ebx,STDIN
mov ecx,n1
mov edx,2
int 0x80

mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,msg2
mov edx,len2
int 0x80

mov eax,SYS_READ
mov ebx,STDIN
mov ecx,n2
mov edx,2
int 0x80
;comparar
mov ax,[n1]
mov bx,[n2]
cmp ax,bx

je iguales
cmp ax,bx

jb mayor
cmp ax,bx

ja menor


iguales:
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,msg5
mov edx,len5
int 0x80
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,salto
mov edx,lensalto
int 0x80
jmp salida


mayor:
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,msg3
mov edx,len3
int 0x80
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,salto
mov edx,lensalto
int 0x80
jmp salida


menor:
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,msg4
mov edx,len4
int 0x80
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,salto
mov edx,lensalto
int 0x80
jmp salida

salida:
mov eax,SYS_EXIT
int 0x80