section .data	;Segmento de datos declarados
msg db "Hola mundo", 0xa,0xd ;Es un salto de linea
len equ $-msg ;Se usa para optener la longitud de un dato

;section .bss	;Segmento de reserve de memoria, datos no declarados

section .text	;Segmento de instrucciones
	global _start ;Es el inicio como el main

_start: ;Indica el inicio de las instrucciones (Siempre sera la mismas sintaxis)


	;Se utiliza para imprimir en pantalla
mov eax,4 ;sys_write
mov ebx,1 ;stdout
mov ecx,msg ;Mensaje que queremos imprimir en pantalla
mov edx,len ;Longitud del mansaje
int 0x80 ;Llamada a la interrupcion

mov eax,1 ;sys_exit
int 0x80 ;LLamada a la interrupcion