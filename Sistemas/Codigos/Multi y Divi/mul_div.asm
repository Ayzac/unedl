section .data
msg1 db "La multiplicacion de 3 * 3 es:"
len1 equ $-msg1

msg2 db "La multiplicacion de -3 * -3 es:"
len2 equ $-msg2

msg3 db "La divicin de 6 / 3 es:"
len3 equ $ -msg3

msg4 db "La devicion de -6 / -3 es:"
len4 equ $ -msg4
salto db " ", 0xa,0xb
lensalto equ $ - salto

section .bss
res resb 1 

section .text
	global _start

_start:
;Impresion del mensaje
	mov eax,4
	mov ebx,1
	mov ecx, msg1
	mov edx, len1
	int 0x80

;Inicial registros
	mov eax,3
	mov ebx,3

;Multiplicacion
	mul ebx ; ebx = eax * ebx

	add eax, '0'; convertirlo a codigo as
	mov [res],eax

	mov eax,4
	mov ebx,1
	mov ecx, res
	mov edx, 1
	int 0x80
	;Imprimir el resultado
	mov eax,4
	mov ebx,1
	mov ecx, salto
	mov edx, lensalto
	int 0x80
	
	;.............Multilicacion con signo...............
	mov eax,4
	mov ebx,1
	mov ecx, msg2
	mov edx, len2
	int 0x80	


	mov eax,-3
	mov ebx,-3
	;iniciarregistros
	imul ebx
	add eax,'0'
	mov [res],eax

	;Imprimir el resultado
	mov eax,4
	mov ebx,1
	mov ecx, res
	mov edx, 1
	int 0x80

	;Imprimir el resultado
	mov eax,4
	mov ebx,1
	mov ecx, salto
	mov edx, lensalto
	int 0x80

;.................DIVICION...............
	mov eax,4
	mov ebx,1
	mov ecx, msg3
	mov edx, len3
	int 0x80

;Inicial registros
	mov eax,6
	mov ebx,3
	mov edx,0

;Divicion
	div ebx ; ebx = eax / ebx

	add eax, '0'; convertirlo a codigo as
	mov [res],eax

	mov eax,4
	mov ebx,1
	mov ecx, res
	mov edx, 1
	int 0x80
	;Imprimir el resultado
	mov eax,4
	mov ebx,1
	mov ecx, salto
	mov edx, lensalto
	int 0x80

;...............DIVICION CON SIGNO............
	mov eax,4
	mov ebx,1
	mov ecx, msg4
	mov edx, len4
	int 0x80

;Inicial registros
	mov eax,-4
	mov ebx,-2
	mov edx,0

;Divicion
	idiv ebx ; ebx = eax / ebx

	add eax, '0'; convertirlo a codigo as
	mov [res],eax

	mov eax,4
	mov ebx,1
	mov ecx, res
	mov edx, 1
	int 0x80
	;Imprimir el resultado
	mov eax,4
	mov ebx,1
	mov ecx, salto
	mov edx, lensalto
	int 0x80

	;Salir
	mov eax,1
	int 0x80