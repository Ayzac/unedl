SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1


section .data
msg1 db "Ingrse un valo : "
len1 equ $- msg1
msg2 db "Ingresa el segundo valor : "
len2 equ $- msg2
msg3 db "El resultado es: "
len3 equ $- msg3
salto db " ",0xa,0xd
lensalto equ $- salto


section .bss
num1 resb 2
num2 resb 2
res resb 1

section .text
	global _start

_start:

;Imprimri el mensaje
	mov eax,SYS_WRITE  ;4
	mov ebx,STDOUT      ;1
	mov ecx,msg1
	mov edx,len1
	int 0x80

;Leer digito
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num1
	mov edx,2
	int 0x80

;Imprimri el mensaje 2
	mov eax,SYS_WRITE  ;4
	mov ebx,STDOUT      ;1
	mov ecx,msg2
	mov edx,len2
	int 0x80

;Leer segundo digito
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num2
	mov edx,2
	int 0x80

;Desconvertir de codigo ascci a un numero decimal 
	mov eax,[num1]
	sub eax,'0' ;Convertimos a numero decimal.

	mov ebx,[num2]
	sub ebx,'0'

;Realizar la suma
	add eax,ebx

	add eax,'0'
	mov [res],eax ;Se guarda el resultado

;imprimir pantalla
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg3
	mov edx,len3
	int 0x80

	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,res
	mov edx,1
	int 0x80

;Saler del programa
	mov eax, SYS_EXIT
	int 0x80