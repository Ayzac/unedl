section .data
msg db "Ingresa un dato",0xa,0xd
len1 equ $- msg
msg2 db "Usted ingreso",0xa,0xd
len2 equ $- msg2

section .bss
dato resb 4

section .text
	global _start
_start:

;Mostrar el mensaje.
	mov eax,4 ;Sys_write
	mov ebx,1 ;stdout
	mov ecx, msg
	mov edx,len1
	int 0x80

;leer dato de teclado
	
	mov eax,3    ;Sys_read
	mov ebx,0    ;stdin
	mov ecx,dato
	mov edx,4    ;la reserba 
	int 0x80

;Mostrar el mensaje.
	mov eax,4 ;Sys_write
	mov ebx,1 ;stdout
	mov ecx, msg2
	mov edx,len2
	int 0x80


	mov eax,4   ;Sys_write
	mov ebx,1    ;stdout
	mov ecx,dato
	mov edx,4    ;la reserba 
	int 0x80

	mov eax,1
	int 0x80