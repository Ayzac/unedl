section .data
msg1 db "Hola  "
len equ $ - msg1
salto db " ",0xa,0xd
lensalto equ $ - salto

section .bss
res resb 3 ;reservar byte para resultado

section .text
  global _start

_start:
;imprimir primer mensaje
mov eax,4
mov ebx,1
mov ecx,msg1
mov edx,len
int 0x80
;Letra Z
mov eax,21
mov ebx,2
mul ebx
add eax,'0';convertir a codigo asc2
mov [res],eax

mov eax,4
mov ebx,1
mov ecx,res
mov edx,1
int 0x80
;letra A
mov eax,10
mov ebx,7
add eax,ebx
add eax,'0';convertir a codigo asc2
mov [res],eax

mov eax,4
mov ebx,1
mov ecx,res
mov edx,1
int 0x80

;letra C
mov eax,10
mov ebx,9
add eax,ebx
add eax,'0';convertir a codigo asc2
mov [res],eax

mov eax,4
mov ebx,1
mov ecx,res
mov edx,1
int 0x80

;salto linea
mov eax,4
mov ebx,1
mov ecx,salto
mov edx,lensalto
int 0x80


mov eax,1
int 0x80