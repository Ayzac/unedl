;Datos
	LIST     P = 16F84A
    INCLUDE <P16F84A.INC>
MASCARA     EQU b'01010101'   	
;Codigo
	ORG   0
Inicio
	bsf   STATUS,RP0    ;Accedemos al Banco 1
	clrf  PORTB		    ;Salida PORTB
	movlw b'11111111'
	movwf PORTA		    ;Entrada PORTA
	bcf   STATUS,RP0    ;Accedemos al banco 0
Principal
	movf  PORTA,W
	xorlw MASCARA
	movwf PORTB     

	goto  Principal

END            