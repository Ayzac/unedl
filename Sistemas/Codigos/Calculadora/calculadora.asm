SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1

section .date
msg db "Calculadora",0xa,0xd
len equ $- msg

msg1 db "Numero 1",0xa,0xd
len1 equ $- msg1

msg0 db "Numero 2",0xa,0xd
len0 equ $- msg0

msg2 db "Selecciona una opcion",0xa,0xd
len2 equ $- msg2

msg3 db "1 Suma",0xa,0xd
len3 equ $- msg3

msg4 db "2 Resta",0xa,0xd
len4 equ $- msg4

msg5 db "3 Multiplicacion",0xa,0xd
len5 equ $- msg5

msg6 db "4 Dividir",0xa,0xd
len6 equ $- msg6

msg7 db "Opcion no valida",0xa,0xd
len7 equ $- msg7

msg8 db "El resultado es :",0xa,0xd
len8 equ $- msg8

msg9 db "Opciones :",0xa,0xd
len9 equ $- msg9

section .bss

opcion resb 2
num1 resb 2
num2 resb 2
resultado resb 2


section .text
	global _start

_start:

;Imprimir mensaje
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg
	mov edx,len
	int 0x80

;Imprimir mensaje
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg1
	mov edx,len1
	int 0x80

;Guardar entrada
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num1
	mov edx,2
	int 0x80

;Imprimir mensaje
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg0
	mov edx,len0
	int 0x80

;Guardar entrada
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num2
	mov edx,2
	int 0x80

;Imprimir mensaje del menu
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg9
	mov edx,len9
	int 0x80

;Imprimir mensaje Suma
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg3
	mov edx,len3
	int 0x80

;Imprimir mensaje Resta
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg4
	mov edx,len4
	int 0x80

;Imprimir mensaje Multiplicacion
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg5
	mov edx,len5
	int 0x80

;Imprimir mensaje Divicion
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg6
	mov edx,len6
	int 0x80

;Imprimir mensaje
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg2
	mov edx,len2
	int 0x80

;Guardar entrada
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,opcion
	mov edx,2
	int 0x80

;Determinar la operacion
	mov ah,[opcion] ;movemos la opcion al registro ah
	sub ah,'0' ;Covertir de ascci a decimal

;Comparacion
	cmp ah,1
	je sumar

	cmp ah,2
	je resta

	cmp ah,3
	je multi

	cmp ah,4
	je division

;Imprimimos mensage de opcion no valida
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg7
	mov edx,len7
	int 0x80

	jmp salida

	sumar:
; Movemos los numeros ingresados a los registro AL y BL
		mov al,[num1]
		mov bl,[num2]

; Convertimos los valores ingresados de ascii a decimal
		sub al,'0'
		sub bl,'0'

; Sumamos el registro AL y BL
		add al,bl  ;al = al+bl

; Convertimos el resultado de la suma de decimal a ascii
		add al,'0'

; Movemos el resultado a un espacio reservado en la memoria
		mov [resultado],al

; Imprimimos en pantalla el mensaje 9
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,msg8
		mov edx,len8
		int 0x80

; Imprimimos en pantalla el resultado
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,resultado
		mov edx,2
		int 0x80

		jmp salida


	resta:
; Movemos los numeros ingresados a los registro AL y BL
		mov al,[num1]
		mov bl,[num2]

; Restar 
		sub al,bl
		add al,'0'  ;al = al-bl


; Movemos el resultado a un espacio reservado en la memoria
		mov [resultado],al

; Imprimimos en pantalla el mensaje 9
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,msg8
		mov edx,len8
		int 0x80

; Imprimimos en pantalla el resultado
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,resultado
		mov edx,2
		int 0x80

		jmp salida


	multi:
; Movemos los numeros ingresados a los registro AL y BL
		mov al,[num1]
		mov bl,[num2]

; Convertimos los valores ingresados de ascii a decimal
		sub al,'0'
		sub bl,'0'

; Sumamos el registro AL y BL
		mul bl  ;ax = bl*al

; Convertimos el resultado de la suma de decimal a ascii
		add ax,'0'

; Movemos el resultado a un espacio reservado en la memoria
		mov [resultado],ax

; Imprimimos en pantalla el mensaje 9
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,msg8
		mov edx,len8
		int 0x80

; Imprimimos en pantalla el resultado
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,resultado
		mov edx,2
		int 0x80

		jmp salida




	division:
; Movemos los numeros ingresados a los registro AL y BL
		mov al,[num1]
		mov bl,[num2]

		mov dx,0
		mov ah,0


		sub al,'0'
		sub bl,'0'

; Sumamos el registro AL y BL
		div bl  ;ax = bl*al

; Convertimos el resultado de la suma de decimal a ascii
		add ax,'0'

; Movemos el resultado a un espacio reservado en la memoria
		mov [resultado],ax

; Imprimimos en pantalla el mensaje 9
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,msg8
		mov edx,len8
		int 0x80

; Imprimimos en pantalla el resultado
		mov eax,SYS_WRITE
		mov ebx,STDOUT
		mov ecx,resultado
		mov edx,2
		int 0x80

		jmp salida

;Saler del programa
	salida:
	mov eax, SYS_EXIT
	int 0x80