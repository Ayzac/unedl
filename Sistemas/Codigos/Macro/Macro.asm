SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1

;macro que imprimi un texto, ocupa texto y longitud de la cadena
%macro imprimir 2
mov eax,SYS_WRITE
mov ebx,STDOUT
mov ecx,%1
mov edx,%2
int 0x80
%endmacro

section .data
msg1 db "ingresa primer numero: ",0xa,0xd
len1 equ $-msg1
msg2 db "ingresa segundo numero: ",0xa,0xd
len2 equ $-msg2
msg3 db "el segundo es mayor "
len3 equ $-msg3
msg4 db "el segundo es menor"
len4  equ $-msg4
msg5 db "son iguales "
len5 equ $-msg5
salto db "",0xa,0xd
lensalto equ $-salto

section .bss
n1 resb 2
n2 resb 2

section .text
leer1:
mov eax,SYS_READ
mov ebx,STDIN
mov ecx,n1
mov edx,2
int 0x80
ret

leer2:
mov eax,SYS_READ
mov ebx,STDIN
mov ecx,n2
mov edx,2
int 0x80
ret

	global _start
_start:
imprimir msg1, len1
call leer1
imprimir msg2,len2
call leer2
mov ax,[n1]
mov bx,[n2]
cmp ax,bx
;salta si son iguales
je iguales
cmp ax,bx
;salta si left<right
jb mayor
cmp ax,bx
;salta si left>right
ja menor

iguales:
imprimir msg5,len5
imprimir salto,lensalto
jmp salida

mayor:
imprimir msg3,len3
imprimir salto,lensalto
jmp salida

menor:
imprimir msg4,len4
imprimir salto,lensalto
jmp salida

salida:
mov eax,SYS_EXIT
int 0x80
