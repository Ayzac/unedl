SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDIN equ 0
STDOUT equ 1


section .data
msg1 db "Ingresa el valor", 0xa,0xd
len1 equ $ -msg1

msg2 db "Es par",0xa,0xd
len2 equ $ -msg2

msg3 db "Es diferente",0xa,0xd
len3 equ $ -msg3


section .bss
num resb 1


section .text
	global _start

_start:

	mov eax,SYS_WRITE
	mov ebx,STDOUT    
	mov ecx,msg1
	mov edx,len1
	int 0x80

	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num 
	mov edx,1
	int 0x80

	mov eax,[num]
	sub eax,'0'

	cmp eax,2
	jz par


	mov eax, SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg3
	mov edx,len3
	int 0x80
	jmp salida
par:	
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg2
	mov edx,len2
	int 0x80
	;jmp salida
salida:
	mov eax,SYS_EXIT
	int 0x80