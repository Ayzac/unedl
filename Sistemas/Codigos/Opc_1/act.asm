section .data
msg db "La suma de 5 y 2 es:", 0xA, 0xD
len equ $ - msg ; Estrae la longitud de msg
salto db " ", 0xa,0xd
lensalto equ $ - salto

;Seccion de variables no declaradas
section .bss
;declarar una ubicacion de memoria que no esta inicializada por medio de una etiqeuta
res resb 1 ;esta instruccion reserva el espacio para colocar el resultado de la operacion

section .text

	global _start

_start:

	;Imprimir pantalla
	mov eax, 4 ;llamda sys_write
	mov ebx, 1 ; stout
	mov ecx, msg 
	mov edx, len
	int 0x80

;Inicializar los registros con los valores
	mov eax, 5
	mov ebx, 2
;realizamos la operacion de suma (ADD)
	add eax, ebx  ;eax = eax + ebx		
;para imprimir el resultado de nuevo necesitamos
;los registros de proposito general

;mov res, eax daria error debido a res 1 byte y eax 4 bytes
	add eax, '0' ;esto es agregar un espacio y conve
	mov [res],eax ;[] =  Es para que acceda al contenido de la etiqueta

;Imprimir resultado de la suma
	mov eax, 4
	mov ebx, 1
	mov ecx, res
	mov edx, 1
	int 0x80

	mov eax, 4
	mov ebx, 1
	mov ecx, salto
	mov edx, lensalto
	int 0x80
;Salida del proceso
	mov eax, 1 ;sys_
	int 0x80