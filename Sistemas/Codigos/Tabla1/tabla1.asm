LIST     P=16F84A
INCLUDE <P16F84A.INC>

ORG 0

Inicio

bsf   STATUS,RP0
clrf  PORTB
movlw b'00011111'
movwf PORTA
bcf   STATUS,RP0

Principal

movf  PORTA
sublw d'8'    ;w=8-PORTA
;si w es positivo es menor a 8 C=1
;si es negativo o 0es mayor a 8 C=0

btfss STATUS,C
goto  Pares
movf  PORTA,W
call  PrenderLeds
goto  Salida

Pares 

movlw b'01010101'

Salida

movwf PORTB
goto  Principal

;subrutina prender leds

PrenderLeds  ;nombre de la subrutina para acceder

addwf PCL,F  ;contador de programa

Tabla

retlw b'0000000'
retlw b'0000001'
retlw b'0000011'
retlw b'0000111'
retlw b'0001111'
retlw b'0011111'
retlw b'0111111'
retlw b'1111111'

END