LIST P=16F84A
INCLUDE <P16F84A.INC>

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00000001'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movlw b'00000001'
	btfsc PORTA,0
	call PrenderRojo
	btfsc PORTA,2
	movwf PORTB
	goto Principal

PrenderRojo
	movlw b'00000100'
	movwf PORTB

Salida
	INCLUDE<RETARDOS.INC>
	END