;Este programa muestra la barra de led
;que conecta el puerto B
;constante b'01010101'

LIST p=16F84A
INCLUDE <P16F84A.INC>

CONSTANTE EQU b'01010101'

;Zona de codigo

	ORG 0
Inicio
	;Acceedemos al banco 1
	bsf STATUS, RP0     ; Establece enc 1 el bit 5 (RP0)del registro status
	clrf	PORTB
	bcf	STATUS, RP0
	movlw	CONSTANTE
Principal
	movwf PORTB
	goto Principal
	END