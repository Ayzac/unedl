;MOstrar por el PORTB lo que se mostrara en el PORTA

LIST p=16F84A
INCLUDE <P16F84A.INC>

;Zona del codigo

	ORG0
Inicio
	bsf STATUS,RP0
	clrf PORTB    ;Configurar como salida
	movlw b'11111111' 	;cargamos el numero en W
	movwf PORTA		;Configuramos como entrada
	bcf STATUS, RP0		;Regresa al banco 0

Principal
	movf  PORTA,W 	;Lee el PUERTA y lo manda a W
	movwf	PORTB
	goto	Principal
	END
	