LIST P=16F84A
INCLUDE <P16F84A.INC>

;zona de codigo
  ORG  0
;configuracion de puertos
Inicio
;bsf envia 1
  bsf   STATUS,RP0  ;acceso al banco 1
  clrf  PORTB
  movlw b'11111111'
  movwf PORTA
  bcf   STATUS,RP0  ;acceso al banco 0
Principal
  comf  PORTA,W
  movwf PORTB
  goto  Principal
END