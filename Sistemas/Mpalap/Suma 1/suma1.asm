; vamos a sumarle una constante a lo que resiva el puerto A

;Zona de datos
	LIST p=16F84A 			;Micorcontrolador que uzamos
	INClUDE <P16F84A.INC>	;Definicion de algunas configuraciones 
CONSTANTE EQU d'74'		;Declaramos la constante


;Zona de codigo
	ORG 0				;Comienza en la direccion 0 del programa.
Inicio						;Indica que configuramos los puertos
	bsf STATUS, RP0		; Establece en 1 el bit de STATUS
	clrf PORTB			;Puerto b de salida
	movlw b'11111111'		
	movwf PORTA			;Puerto A como entrada
	bcf STATUS,RP0		;Establece en o el bit 5 de STATUS.
Principal					;Indica qu comenzamos con las operaciones
	movf PORTA,W			;Carga el registro W con los datos del puerto A
	addlw CONSTANTE		;W = (PORTA) + CONSTANTE
	movwf PORTB			;Deposita  el resultado de la suma
	goto Principal			;Bucle cerrado infinito
	
	END					;Termina