;Mi primer Hola Mundo

;Zana de los datos

LIST p=16F84A          ;permite indicar el tipo de PIC    "Siempre usarlo"
INCLUDE <P16F84A.INC>  ; Declamaros para incluir el archivo .INC

;Zona de codigo
	ORG 0   	;Indicar que comience en la direccion 0 de la memoria del programa
;El bit 5 = RBO del registro STATUS, 
	bsf STATUS,RP0	;Establece en 1 el bit 5 (RP0) del registro STATUS
 ;Permite seleccionar el banco 1
;Los puertos se configuran en el Banco 1
;Si PORTB/PORTA = 0, Se configura como salida
;Si PORTB/PORTA = 1, Se configura como entrada
	clrf PORTB	;Configura como salida el PORTB/TRISB establecido en 0  todos sus bits
	bcf STATUS,RP0 	;Establecer en 0 el bit 5(RP0) del registro STATUS selecciona el banco 0 
	bsf PORTB,0 ;bit 0= RB0, enviar 1 es una se�al para encender el led
	END