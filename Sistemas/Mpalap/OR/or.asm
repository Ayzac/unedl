;zona de datos
LIST p=16F84A
INCLUDE <P16F84A.INC>

MASCARA EQU b'01010101';mascara de bit pares siempre en 1
;ZOna de codigo
ORG 0

Inicio
   bsf   STATUS,RP0 ;acceso al banco 1
   clrf  PORTB     ;PORTB como salida
   movlw b'11111111' ;
   movwf PORTA     ;PORTA entrada
   bcf   STATUS,RP0 ;acceso al banco 0
Principal
;---11111
;01010101
   movf  PORTA,W   ;carga el registro w con los datos PORTA
   iorlw MASCARA   ;
   movwf PORTB
   goto  Principal
END 