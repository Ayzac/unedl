;Mostrar por el PORTB, el PORTA multiplicado por 2
;PORTB = 2(PORTA) = 	PORTA + PORTA

	LIST  p=16F84A
	INCLUDE <P16F84A.INC>

	ORG 0

Inicio
	bsf STATUS,RP0
	clrf	PORTB
	movlw b'11111111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movf PORTA,W			;W = PORTA
	addwf PORTA, W		;W + PORTA + PORTA
	movwf PORTB
	goto Principal

	END