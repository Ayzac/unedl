LIST P = 16F84A
	INCLUDE<P16F84A.INC> 

	ORG 0
	
Inicio
	clrf PORTB 					;Limpia el PORTB
	bsf STATUS,RP0 				;PORTB  salida
	clrf PORTB
	movlw b'00001111' 			
	movwf PORTA	
	bcf STATUS,RP0
	
Principal
	movf PORTA,W	
	andlw b'00001111';			
	addwf PCL,F
	
;combinaciones
	goto Configuracion0			
	goto Configuracion1
	goto Configuracion2
	goto Configuracion3
	goto Configuracion4
	goto Configuracion5
	goto Configuracion6
	goto Configuracion7
	goto Configuracion8
	goto Configuracion9
	goto Configuracion10
	goto Configuracion11
	goto Configuracion12
	goto Configuracion13
	goto Configuracion14
	goto Configuracion15
	
Configuracion0
	movlw b'00000001'
	goto Salida
	
Configuracion1
	movlw b'00000011'
	goto Salida
	
Configuracion2
	movlw b'00000100'
	goto Salida
	
Configuracion3
	movlw b'00111111'
	goto Salida
	
Configuracion4
	movlw b'00001100'
	goto Salida
	
Configuracion5
	movlw b'00111111'
	goto Salida
	
Configuracion6
	movlw b'00111111'
	goto Salida
	
Configuracion7
	movlw b'00111111'
	goto Salida
	
Configuracion8
	movlw b'00110000'
	goto Salida
Configuracion9
	movlw b'00111111'
	goto Salida
	
Configuracion10
	movlw b'00111111'
	goto Salida
	
Configuracion11
	movlw b'00111111'
	goto Salida
	
Configuracion12
	movlw b'00111111'
	goto Salida
	
Configuracion13
	movlw b'00111111'
	goto Salida
Configuracion14
	movlw b'00111111'
	goto Salida
Configuracion15
	movlw b'00111111'		
	goto Salida
	
Salida									
	movwf PORTB	
	goto Principal					
	END